# README #

This is a Web API developed using .NET Core 3.1, which supports the following,

1. Endpoint: User Create - Route: "/api/users/create", Verb: POST

2. Endpoint: User List - Route: "/api/users", Verb: GET

### Highlights

* Both database and WebAPI are hosted in Azure
* Clean Architecture - CQRS 
* Clean Code
* Integration Tests  
* Swagger UI

### How to run the application

1. Clone/download the source code
2. Make sure "CodeChallenge.WebAPI" is set as the startup project
3. Run (IIS Express)!
4. Swagger UI will load on the browser.
5. This codebase connects to a Microsoft Azure SQL Database. 

### How to connect to a different database server

1. Create an empty database.
2. Execute 'createTable.sql' script.
2. Set connection string (in appsettings.json).
3. Run!

### How to run integration tests
1. Run tests in 'CodeChallenge.WebAPI.IntegrationTests'

### Furthur Improvements (These can be added if required)
* Add Authentication/Logging
* Unit tests
* Exception filter

### Known deviations from the provided sample data  
* 'PostCode' is a string literal (not a numeric value) since it is not a number in certain countries.



