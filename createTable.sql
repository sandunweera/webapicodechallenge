CREATE TABLE [dbo].[Users](
	[Gender] [nvarchar](50) NULL,
	[Name] [nvarchar](250) NULL,
	[Street] [nvarchar](250) NULL,
	[City] [nvarchar](250) NULL,
	[State] [nvarchar](250) NULL,
	[Country] [nvarchar](250) NULL,
	[PostCode] [nvarchar](50) NULL,
	[Email] [nvarchar](250) NULL,
	[Username] [nvarchar](250) NULL,
	[Password] [nvarchar](250) NULL,
	[DOB] [datetime] NULL,
	[Phone] [nvarchar](250) NULL,
	[Cell] [nvarchar](250) NULL,
	[LargePicture] [nvarchar](3000) NULL,
	[MediumPicture] [nvarchar](3000) NULL,
	[ThumbnailPicture] [nvarchar](3000) NULL,
	[Nat] [nvarchar](50) NULL
) 
GO