﻿namespace CodeChallenge.Abstractions.CQRS
{
    /// <summary>
    ///     A marker interface to be implemented by all commands.
    /// </summary>
    public interface ICommand
    {
    }
}