﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CodeChallenge.Abstractions
{
    /// <summary>
    ///     Abstraction for the data storage.
    /// </summary>
    public interface IDataStore<T>
    {
        Task CreateAsync(T item);

        Task<IEnumerable<T>> GetAllAsync();
    }
}