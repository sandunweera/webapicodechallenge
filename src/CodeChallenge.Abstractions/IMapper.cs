﻿namespace CodeChallenge.Abstractions
{
    /// <summary>
    ///     Represents a class that is capable of mapping a <typeparamref name="TI" /> to <typeparamref name="TO" />.
    /// </summary>
    public interface IMapper<in TI, out TO>
    {
        /// <summary>
        ///     Generic method to define the mapping of input type to output type
        /// </summary>
        /// <param name="input">Source object</param>
        /// <returns>Mapped object</returns>
        TO Map(TI input);
    }
}