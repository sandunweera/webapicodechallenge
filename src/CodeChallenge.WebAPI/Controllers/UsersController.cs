﻿using System.Threading.Tasks;
using CodeChallenge.Abstractions.CQRS;
using CodeChallenge.Command;
using CodeChallenge.Models;
using CodeChallenge.Models.Dto;
using CodeChallenge.Query;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CodeChallenge.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly ICommandHandler<AddUserCommand> _addUserCommandHandler;
        private readonly IQueryHandler<GetAllUsersQuery, UserDto[]> _allUsersQueryHandler;
        private readonly IQueryHandler<GetRandomUserQuery, User> _randomUserQueryHandler;

        public UsersController(IQueryHandler<GetRandomUserQuery, User> randomUserQueryHandler,
            IQueryHandler<GetAllUsersQuery, UserDto[]> allUsersQueryHandler,
            ICommandHandler<AddUserCommand> addUserCommandHandler)
        {
            _randomUserQueryHandler = randomUserQueryHandler;
            _allUsersQueryHandler = allUsersQueryHandler;
            _addUserCommandHandler = addUserCommandHandler;
        }

        /// <summary>
        ///     Fetch a random user from https://randomuser.me/api/ and save it to the data storage.
        /// </summary>
        /// <returns></returns>
        [HttpPost("Create")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> Create()
        {
            var user = await _randomUserQueryHandler.HandleAsync(new GetRandomUserQuery());
            if (user == null)
            {
                ModelState.AddModelError("", "Error invoking https://randomuser.me/api/");
                return StatusCode(500, ModelState);
            }

            await _addUserCommandHandler.HandleAsync(new AddUserCommand(user));
            return Ok();
        }

        /// <summary>
        ///     Get all users from the data storage.
        /// </summary>
        /// <returns>List of users.</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(UserDto[]))]
        public async Task<IActionResult> Get()
        {
            var users = await _allUsersQueryHandler.HandleAsync(new GetAllUsersQuery());
            return Ok(users);
        }
    }
}