using System;
using System.IO;
using System.Reflection;
using CodeChallenge.Abstractions;
using CodeChallenge.Abstractions.CQRS;
using CodeChallenge.Command;
using CodeChallenge.CommandHandler;
using CodeChallenge.Models;
using CodeChallenge.Models.Dto;
using CodeChallenge.Query;
using CodeChallenge.QueryHandler;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace CodeChallenge.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = Configuration.GetConnectionString("DefaultConnection");

            services.AddControllers();
            services.AddScoped<IDataStore<User>>(s => new SqlDataStore(connectionString));
            services.AddScoped<IMapper<User, UserDto>, UserMapper>();
            services.AddScoped<IMapper<Result, User>, ResultMapper>();

            services.AddScoped<ICommandHandler<AddUserCommand>, AddUserCommandHandler>();
            services.AddScoped<IQueryHandler<GetAllUsersQuery, UserDto[]>, GetAllUsersQueryHandler>();
            services.AddScoped<IQueryHandler<GetRandomUserQuery, User>, GetRandomUserQueryHandler>();

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("CodeChallenge.WebAPI",
                    new OpenApiInfo
                    {
                        Title = "CodeChallenge.WebAPI",
                        Version = "1"
                    });
                var xmlCommentFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var cmlCommentsFullPath = Path.Combine(AppContext.BaseDirectory, xmlCommentFile);
                options.IncludeXmlComments(cmlCommentsFullPath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();

            app.UseHttpsRedirection();
            app.UseSwagger();

            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/CodeChallenge.WebAPI/swagger.json", "CodeChallenge.WebAPI");
                options.RoutePrefix = "";
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}