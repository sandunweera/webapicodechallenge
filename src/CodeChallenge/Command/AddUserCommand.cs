﻿using CodeChallenge.Abstractions.CQRS;
using CodeChallenge.Models;

namespace CodeChallenge.Command
{
    public class AddUserCommand : ICommand
    {
        public AddUserCommand(User user)
        {
            User = user;
        }

        public User User { get; set; }
    }
}