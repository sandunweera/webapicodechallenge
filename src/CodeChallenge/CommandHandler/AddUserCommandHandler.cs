﻿using System.Threading.Tasks;
using CodeChallenge.Abstractions;
using CodeChallenge.Abstractions.CQRS;
using CodeChallenge.Command;
using CodeChallenge.Models;

namespace CodeChallenge.CommandHandler
{
    public class AddUserCommandHandler : ICommandHandler<AddUserCommand>
    {
        private readonly IDataStore<User> _dataStore;

        public AddUserCommandHandler(IDataStore<User> dataStore)
        {
            _dataStore = dataStore;
        }

        public async Task HandleAsync(AddUserCommand command)
        {
            await _dataStore.CreateAsync(command.User);
        }
    }
}