﻿using System;

namespace CodeChallenge.Models
{
    public class Dob
    {
        public DateTime Date { get; set; }
    }
}