﻿namespace CodeChallenge.Models.Dto
{
    public class PictureDto
    {
        public string Large { get; set; }
        public string Medium { get; set; }
        public string Thumbnail { get; set; }
    }
}