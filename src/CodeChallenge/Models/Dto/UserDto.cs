﻿using System;

namespace CodeChallenge.Models.Dto
{
    public class UserDto
    {
        public string Gender { get; set; }
        public string Name { get; set; }
        public AddressDto Address { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public DateTime Dob { get; set; }
        public string Phone { get; set; }
        public string Cell { get; set; }
        public PictureDto Picture { get; set; }
        public string Nat { get; set; }
    }
}