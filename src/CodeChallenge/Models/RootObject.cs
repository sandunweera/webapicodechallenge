﻿using System.Collections.Generic;

namespace CodeChallenge.Models
{
    public class RootObject
    {
        public List<Result> Results { get; set; }
    }
}