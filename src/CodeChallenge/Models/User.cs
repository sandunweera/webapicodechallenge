﻿using System;
using Dapper.Contrib.Extensions;

namespace CodeChallenge.Models
{
    public class User
    {
        public string Gender { get; set; }
        public string Name { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PostCode { get; set; }
        public string Email { get; set; }
        [ExplicitKey] public string Username { get; set; }
        public string Password { get; set; }
        public DateTime Dob { get; set; }
        public string Phone { get; set; }
        public string Cell { get; set; }
        public string LargePicture { get; set; }
        public string MediumPicture { get; set; }
        public string ThumbnailPicture { get; set; }
        public string Nat { get; set; }
    }
}