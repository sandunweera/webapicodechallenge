﻿using CodeChallenge.Abstractions.CQRS;
using CodeChallenge.Models.Dto;

namespace CodeChallenge.Query
{
    public class GetAllUsersQuery : IQuery<UserDto[]>
    {
    }
}