﻿using CodeChallenge.Abstractions.CQRS;
using CodeChallenge.Models;

namespace CodeChallenge.Query
{
    public class GetRandomUserQuery : IQuery<User>
    {
    }
}