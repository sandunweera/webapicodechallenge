﻿using System.Linq;
using System.Threading.Tasks;
using CodeChallenge.Abstractions;
using CodeChallenge.Abstractions.CQRS;
using CodeChallenge.Models;
using CodeChallenge.Models.Dto;
using CodeChallenge.Query;

namespace CodeChallenge.QueryHandler
{
    public class GetAllUsersQueryHandler : IQueryHandler<GetAllUsersQuery, UserDto[]>
    {
        private readonly IDataStore<User> _dataStore;
        private readonly IMapper<User, UserDto> _userMapper;

        public GetAllUsersQueryHandler(IDataStore<User> dataStore, IMapper<User, UserDto> userMapper)
        {
            _dataStore = dataStore;
            _userMapper = userMapper;
        }

        public async Task<UserDto[]> HandleAsync(GetAllUsersQuery query)
        {
            var result = await _dataStore.GetAllAsync();
            return result.Select(r => _userMapper.Map(r)).ToArray();
        }
    }
}