﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;
using CodeChallenge.Abstractions;
using CodeChallenge.Abstractions.CQRS;
using CodeChallenge.Models;
using CodeChallenge.Query;
using Newtonsoft.Json;
using RestSharp;

namespace CodeChallenge.QueryHandler
{
    public class GetRandomUserQueryHandler : IQueryHandler<GetRandomUserQuery, User>
    {
        private readonly RestClient _client;
        private readonly IMapper<Result, User> _resultMapper;
        private readonly string _url = "https://randomuser.me/api/";

        public GetRandomUserQueryHandler(IMapper<Result, User> resultMapper)
        {
            _resultMapper = resultMapper;
            _client = new RestClient(_url);
        }

        public async Task<User> HandleAsync(GetRandomUserQuery query)
        {
            var request = new RestRequest(_url, Method.GET);
            var response = await _client.ExecuteAsync(request);

            if (response.StatusCode != HttpStatusCode.OK) return null;

            var rootObject = JsonConvert.DeserializeObject<RootObject>(response.Content);
            var user = rootObject.Results.FirstOrDefault();

            return _resultMapper.Map(user);
        }
    }
}