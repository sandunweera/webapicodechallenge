﻿using CodeChallenge.Abstractions;
using CodeChallenge.Models;

namespace CodeChallenge
{
    public class ResultMapper : IMapper<Result, User>
    {
        public User Map(Result input)
        {
            var user = new User
            {
                City = input.Location.City,
                PostCode = input.Location.PostCode,
                State = input.Location.State,
                Country = input.Location.Country,
                Street = $"{input.Location.Street.Number} {input.Location.Street.Name}",
                Cell = input.Cell,
                Dob = input.Dob.Date,
                Email = input.Email,
                Gender = input.Gender,
                Name = $"{input.Name?.Title}. {input.Name?.First} {input.Name?.Last}",
                Password = input.Login.Password,
                LargePicture = input.Picture.Large,
                MediumPicture = input.Picture.Medium,
                ThumbnailPicture = input.Picture.Thumbnail,
                Username = input.Login.Username,
                Phone = input.Phone,
                Nat = input.Nat
            };

            return user;
        }
    }
}