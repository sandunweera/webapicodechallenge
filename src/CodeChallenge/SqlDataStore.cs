﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using CodeChallenge.Abstractions;
using CodeChallenge.Models;
using Dapper.Contrib.Extensions;

namespace CodeChallenge
{
    public class SqlDataStore : IDataStore<User>
    {
        private readonly string _connectionString;

        public SqlDataStore(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task CreateAsync(User item)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.InsertAsync(item);
            }
        }

        public async Task<IEnumerable<User>> GetAllAsync()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return await connection.GetAllAsync<User>();
            }
        }
    }
}