﻿using CodeChallenge.Abstractions;
using CodeChallenge.Models;
using CodeChallenge.Models.Dto;

namespace CodeChallenge
{
    public class UserMapper : IMapper<User, UserDto>
    {
        public UserDto Map(User input)
        {
            var userDto = new UserDto
            {
                Address = new AddressDto
                {
                    City = input.City,
                    PostCode = input.PostCode,
                    State = input.State,
                    Country = input.Country,
                    Street = input.Street
                },
                Cell = input.Cell,
                Dob = input.Dob,
                Email = input.Email,
                Gender = input.Gender,
                Name = input.Name,
                Password = input.Password,
                Picture = new PictureDto
                {
                    Large = input.LargePicture, Medium = input.MediumPicture, Thumbnail = input.ThumbnailPicture
                },
                Username = input.Username,
                Phone = input.Phone,
                Nat = input.Nat
            };

            return userDto;
        }
    }
}