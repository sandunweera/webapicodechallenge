﻿using System.Net.Http;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;

namespace CodeChallenge.WebAPI.IntegrationTests
{
    public class TestClientProvider
    {
        public TestClientProvider()
        {
            var server = new TestServer(WebHost.CreateDefaultBuilder()
                .UseStartup<Startup>()
            );

            Client = server.CreateClient();
        }

        public HttpClient Client { get; }
    }
}