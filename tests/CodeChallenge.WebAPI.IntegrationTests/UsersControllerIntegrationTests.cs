using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace CodeChallenge.WebAPI.IntegrationTests
{
    public class UsersControllerIntegrationTests
    {
        [Fact]
        public async Task Test_Create()
        {
            using (var client = new TestClientProvider().Client)
            {
                var response = await client.PostAsync("/api/users/create", null);

                response.EnsureSuccessStatusCode();

                Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            }
        }

        [Fact]
        public async Task Test_GetAll()
        {
            using (var client = new TestClientProvider().Client)
            {
                var response = await client.GetAsync("/api/users");

                response.EnsureSuccessStatusCode();

                Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            }
        }
    }
}